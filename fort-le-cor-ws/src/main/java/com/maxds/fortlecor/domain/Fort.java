package com.maxds.fortlecor.domain;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@Component
public class Fort {

    private final Integer pointsDeVieMaxi = 20000;
    private Integer pointsDeVieRestants = 20000;

    public void ajout(Integer pointsDeVie) {
        pointsDeVieRestants += pointsDeVie;
        if (pointsDeVieRestants > pointsDeVieMaxi) {
            pointsDeVieRestants = pointsDeVieMaxi;
        }
    }

    public void retrait(Integer pointsDeVie) {
        pointsDeVieRestants -= pointsDeVie;
        if (pointsDeVieRestants < 0) {
            pointsDeVieRestants = 0;
        }
    }

}