package com.maxds.fortlecor.controller;

import java.util.Random;

import com.maxds.fortlecor.domain.Fort;
import com.maxds.fortlecor.domain.ResultatIntrusion;
import com.maxds.fortlecor.domain.TentativeIntrusion;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@RequestMapping("/fort")
@Slf4j
public class FortController {

	private final Fort fort;

	@PostMapping("/attaque")
	public ResultatIntrusion tentative(@RequestBody TentativeIntrusion tentativeIntrusion) throws Exception {

		Integer coutAttaque = 0;

		switch (tentativeIntrusion.getTypeAssaillant()) {
		case EXPLOSIF:
			coutAttaque = 3;
			break;
		case BELIER:
			coutAttaque = 2;
			break;
		case URUK_HAI:
			coutAttaque = 15;
			break;
		case ORQUE:
			coutAttaque = 8;
			break;
		default:
			coutAttaque = 0;
			break;
		}

		fort.retrait(coutAttaque);

		if (fort.getPointsDeVieRestants() == 0) {
			throw new Exception("Le fort est tombé, faites vos prières !!");
		}
		Thread.sleep(new Random().nextInt(100));
		log.info("Attaque du fort encaissée, points de vie : {}/{}", fort.getPointsDeVieRestants(), fort.getPointsDeVieMaxi());
		return new ResultatIntrusion(true, fort.getPointsDeVieRestants());
	}

	@GetMapping("/status")
	public Fort status() {
		return fort;
	}

}