package com.maxds.fortlecor.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TentativeIntrusion {

    private TypeAssaillant typeAssaillant;

}