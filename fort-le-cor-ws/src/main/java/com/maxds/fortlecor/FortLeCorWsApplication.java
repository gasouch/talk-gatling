package com.maxds.fortlecor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FortLeCorWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(FortLeCorWsApplication.class, args);
	}

}
