package com.maxds.fortlecor.domain;

public enum TypeAssaillant {
    BELIER, URUK_HAI, ORQUE, EXPLOSIF
}