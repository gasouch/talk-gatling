package com.maxds.fortlecor.controller;

import java.util.Random;

import com.maxds.fortlecor.domain.Mur;
import com.maxds.fortlecor.domain.ResultatIntrusion;
import com.maxds.fortlecor.domain.TentativeIntrusion;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@RequestMapping("/mur")
@Slf4j
public class MurController {

	private final Mur mur;

	@PostMapping("/attaque")
	public ResultatIntrusion tentative(@RequestBody TentativeIntrusion tentativeIntrusion) throws Exception {

		Integer coutAttaque = 0;

		switch (tentativeIntrusion.getTypeAssaillant()) {
		case EXPLOSIF:
			coutAttaque = 30;
			break;
		case BELIER:
			coutAttaque = 0;
			break;
		case URUK_HAI:
			coutAttaque = 2;
			break;
		case ORQUE:
			coutAttaque = 1;
			break;
		default:
			coutAttaque = 0;
			break;
		}

		mur.retrait(coutAttaque);

		if (mur.getPointsDeVieRestants() == 0) {
			throw new Exception("Brêche dans le mur, repliez-vous au fort !!");
		}
		Thread.sleep(new Random().nextInt(100));
		log.info("Attaque du mur encaissée, points de vie : {}/{}", mur.getPointsDeVieRestants(), mur.getPointsDeVieMaxi());
		return new ResultatIntrusion(true, mur.getPointsDeVieRestants());
	}

	@GetMapping("/status")
	public Mur status() {
		return mur;
	}

}