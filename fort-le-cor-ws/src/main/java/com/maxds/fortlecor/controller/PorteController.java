package com.maxds.fortlecor.controller;

import java.util.Random;

import com.maxds.fortlecor.domain.Porte;
import com.maxds.fortlecor.domain.ResultatIntrusion;
import com.maxds.fortlecor.domain.TentativeIntrusion;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@RequestMapping("/porte")
@Slf4j
public class PorteController {

	private final Porte porte;

	@PostMapping("/attaque")
	public ResultatIntrusion tentative(@RequestBody TentativeIntrusion tentativeIntrusion) throws Exception {

		Integer coutAttaque = 0;

		switch (tentativeIntrusion.getTypeAssaillant()) {
		case EXPLOSIF:
			coutAttaque = 8;
			break;
		case BELIER:
			coutAttaque = 20;
			break;
		case URUK_HAI:
			coutAttaque = 3;
			break;
		case ORQUE:
			coutAttaque = 2;
			break;
		default:
			coutAttaque = 0;
			break;
		}

		porte.retrait(coutAttaque);

		if (porte.getPointsDeVieRestants() == 0) {
			throw new Exception("Brêche dans la porte, appelez du renfort !!");
		}
		Thread.sleep(new Random().nextInt(100));
		log.info("Attaque de la porte encaissée, points de vie : {}/{}", porte.getPointsDeVieRestants(), porte.getPointsDeVieMaxi());
		return new ResultatIntrusion(true, porte.getPointsDeVieRestants());
	}

	@PostMapping("/reparation")
	public Porte reparation(@RequestParam(name = "pointsDeVie") Integer pointsDeVie) {
		porte.ajout(pointsDeVie);
		return porte;
	}

	@GetMapping("/status")
	public Porte status() {
		return porte;
	}

}