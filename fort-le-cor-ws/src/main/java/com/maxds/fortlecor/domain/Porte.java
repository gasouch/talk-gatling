package com.maxds.fortlecor.domain;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@Component
public class Porte {

    private final Integer pointsDeVieMaxi = 5000;
    private Integer pointsDeVieRestants = 5000;

    public void ajout(Integer pointsDeVie) {
        pointsDeVieRestants += pointsDeVie;
        if (pointsDeVieRestants > pointsDeVieMaxi) {
            pointsDeVieRestants = pointsDeVieMaxi;
        }
    }

    public void retrait(Integer pointsDeVie) {
        pointsDeVieRestants -= pointsDeVie;
        if (pointsDeVieRestants < 0) {
            pointsDeVieRestants = 0;
        }
    }

}