import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class BatailleDuGouffreDeHelm extends Simulation {

  val httpProtocol = http
    .baseUrl("http://localhost:8080")
    .inferHtmlResources()
    .acceptHeader("*/*")

  val armeeSaroumane = csv("armeeSaroumane.csv").random

  val scenarioPorte = scenario("scénario de l'attaque de la porte")
    .feed(armeeSaroumane)
    .exec(
      http("Attaque de la porte")
        .post("/porte/attaque")
        .body(StringBody("""{"typeAssaillant": "${Classe}"}"""))
        .headers(Map("Content-Type" -> "application/json"))
    )

  val scenarioMur = scenario("scénario de l'attaque du mur")
    .feed(armeeSaroumane)
    .exec(
      http("Attaque du mur")
        .post("/mur/attaque")
        .body(StringBody("""{"typeAssaillant": "${Classe}"}"""))
        .headers(Map("Content-Type" -> "application/json"))
    )

  val scenarioFort = scenario("scénario de l'attaque du fort")
    .feed(armeeSaroumane)
    .exec(
      http("Attaque du fort")
        .post("/fort/attaque")
        .body(StringBody("""{"typeAssaillant": "${Classe}"}"""))
        .headers(Map("Content-Type" -> "application/json"))
    )

  setUp(
    scenarioPorte.inject(
      // 0 -> 10 sec : montée progressive pour atteindre un débit de 10 requêtes/seconde
      rampUsersPerSec(0) to 10 during (10 seconds),
      // 10 -> 40 sec : maintien de la charge de 10 requêtes/seconde.
      constantUsersPerSec(10) during (30 seconds),
      // 40 -> 50 sec : montée progressive pour atteindre un débit de 25 requêtes/seconde
      rampUsersPerSec(10) to 25 during (10 seconds),
      // 50 -> 60 sec : maintien de la charge de 25 requêtes/seconde.
      constantUsersPerSec(25) during (10 seconds),
      // 60 -> 90 sec : descente progressive pour atteindre un débit de 5 requêtes/seconde
      rampUsersPerSec(25) to 5 during (30 seconds),
      // 90 -> 120 sec : descente progressive pour atteindre un débit de 0 requêtes/seconde
      rampUsersPerSec(5) to 0 during (30 seconds),
    ).protocols(httpProtocol),

    scenarioMur.inject(
      // 0 -> 15 sec : montée progressive pour atteindre un débit de 30 requêtes/seconde
      rampUsersPerSec(0) to 30 during (15 seconds),
      // 15 -> 75 sec : maintien de la charge de 30 requêtes/seconde.
      constantUsersPerSec(30) during (1 minutes),
      // 75 -> 120 sec : descente progressive pour atteindre un débit de 0 requêtes/seconde
      rampUsersPerSec(30) to 0 during (45 seconds),
    ).protocols(httpProtocol),

    scenarioFort.inject(
      // 0 -> 30 sec : on envoie rien du tout pendant 30 secondes
      constantUsersPerSec(0) during (30 seconds),
      // 30 -> 40 sec : montée progressive pour atteindre un débit de 30 requêtes/seconde
      rampUsersPerSec(0) to 30 during (10 seconds),
      // 40 -> 45 sec : descente progressive pour atteindre un débit de 0 requêtes/seconde
      rampUsersPerSec(30) to 0 during (5 seconds),
      // 45 -> 55 sec : montée progressive pour atteindre un débit de 30 requêtes/seconde
      rampUsersPerSec(0) to 30 during (10 seconds),
      // 55 -> 60 sec : descente progressive pour atteindre un débit de 0 requêtes/seconde
      rampUsersPerSec(30) to 0 during (5 seconds),
      // 60 -> 70 sec : montée progressive pour atteindre un débit de 30 requêtes/seconde
      rampUsersPerSec(0) to 30 during (10 seconds),
      // 70 -> 75 sec : descente progressive pour atteindre un débit de 0 requêtes/seconde
      rampUsersPerSec(30) to 0 during (5 seconds),
      // 75 -> 85 sec : montée progressive pour atteindre un débit de 30 requêtes/seconde
      rampUsersPerSec(0) to 30 during (10 seconds),
      // 85 -> 90 sec : descente progressive pour atteindre un débit de 0 requêtes/seconde
      rampUsersPerSec(30) to 0 during (5 seconds),
      // 90 -> 100 sec : montée progressive pour atteindre un débit de 30 requêtes/seconde
      rampUsersPerSec(0) to 30 during (10 seconds),
      // 100 -> 105 sec : descente progressive pour atteindre un débit de 0 requêtes/seconde
      rampUsersPerSec(30) to 0 during (5 seconds),
      // 105 -> 115 sec : montée progressive pour atteindre un débit de 30 requêtes/seconde
      rampUsersPerSec(0) to 30 during (10 seconds),
      // 115 -> 120 sec : descente progressive pour atteindre un débit de 0 requêtes/seconde
      rampUsersPerSec(30) to 0 during (5 seconds),
    ).protocols(httpProtocol),

  )
}
