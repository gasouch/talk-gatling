package com.maxds.fortlecor.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResultatIntrusion {

    private Boolean resultat;
    private Integer pointsDeVieRestants;

}